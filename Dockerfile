FROM openjdk:8-jdk-alpine3.9
ARG commit_id
ENV version=$commit_id
RUN  mkdir /u01 &&  addgroup -S scott && adduser -S  scott -G  scott  && chown -R scott:scott /u01
COPY ["target/assignment-$commit_id.jar","entrypoint.sh", "/u01/"]
USER scott
WORKDIR /u01
EXPOSE 8090
#CMD ["sh", "-c", "java -jar assignment-$version.jar"]
CMD java -jar assignment-$version.jar
#ENTRYPOINT ["/u01/entrypoint.sh"]


#CMD ["executable","param1","param2"]
# CMD java -jar assignment-$version.jar  -- this command works


